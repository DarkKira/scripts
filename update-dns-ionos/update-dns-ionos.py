import logging
import requests
import os
from dotenv import load_dotenv

load_dotenv()
LOG_FILE = os.getenv("LOG_FILE")
IP_FILE = os.getenv("IP_FILE")
IONOS_BASE_URL = os.getenv("IONOS_BASE_URL")
GET_IP_URL = os.getenv("GET_IP_URL")
DNS_RECORD_BODY_DISABLED = os.getenv("DNS_RECORD_BODY_DISABLED")
DNS_RECORD_BODY_TTL = os.getenv("DNS_RECORD_BODY_TTL")
DNS_RECORD_BODY_PRIO = os.getenv("DNS_RECORD_BODY_PRIO")
ROOT_NAME = os.getenv("ROOT_NAME")
RECORD_NAMES = os.getenv("RECORD_NAMES")
API_PREFIX = os.getenv("API_PREFIX")
API_KEY = os.getenv("API_KEY")

RECORD_NAMES = RECORD_NAMES.split(",")

API_KEY_HEADER = {"X-API-Key": f"{API_PREFIX}.{API_KEY}"}
DNS_ZONES_URL = f"{IONOS_BASE_URL}dns/v1/zones"

DNS_RECORD_BODY_1 = f'{{"disabled":{DNS_RECORD_BODY_DISABLED},"content":"'
DNS_RECORD_BODY_2 = f'","ttl":{DNS_RECORD_BODY_TTL},"prio":{DNS_RECORD_BODY_PRIO}}}'


logging.basicConfig(filename=LOG_FILE, format="%(asctime)s|%(process)d|%(levelname)s|%(message)s")
log = logging.getLogger()
log.setLevel(logging.INFO)


def get_pub_ip():
    """Docstring: Gets the public ip address of the host machine. """

    return requests.get(GET_IP_URL).text


def get_ip_change():
    """Docstring: Return changed ip address. """

    pub_ip = get_pub_ip()
    try:
        file = open(IP_FILE, "r")
    except FileNotFoundError:
        log.warning("File pub-ip.txt does not exit! Creating it now.")
        file = open(IP_FILE, "w")
        file.write(pub_ip)
        file.close()
        return pub_ip
    cur_ip = file.read()
    file.close()
    if cur_ip == pub_ip:
        log.info("Nothing to change")
        exit(0)
    log.info(f"IP is now {pub_ip}")
    file = open(IP_FILE, "w")
    file.write(pub_ip)
    file.close()
    return pub_ip


def get_dns_zone_id():
    """Docstring: Gets the dns zone id. """

    zone_request = requests.get(DNS_ZONES_URL, headers=API_KEY_HEADER)
    data = zone_request.json()
    zone_id = data[0]['id']
    return zone_id


def get_dns_record(zone_id, name):
    """Docstring: Gets the dns record. """

    request = requests.get(f"{DNS_ZONES_URL}/{zone_id}", headers=API_KEY_HEADER, params={"recordName": name})
    data: dict = request.json()
    record: dict = data['records'][0]
    return record


def change_dns_record(zone_id, record_id, ip):
    """Docstring: Changes the dns record for the given record name. """

    body = DNS_RECORD_BODY_1 + ip + DNS_RECORD_BODY_2
    header = {"X-API-Key": f"{API_PREFIX}.{API_KEY}", "accept": "application/json", "Content-Type": "application/json"}
    request = requests.put(f"{DNS_ZONES_URL}/{zone_id}/records/{record_id}", data=body, headers=header)
    if request.status_code == 200:
        log.info(f"Changed IP of Record with ID: {record_id} to {ip}")
    else:
        log.error(f"Could not change IP of Record with ID: {record_id} - HTTP {str(request.status_code)} "
                  f"Response:{str(request.json()[0])}")


def main():
    """Docstring: Is here to start this module from the console or shell. """

    pub_ip = get_ip_change()
    zone_id = get_dns_zone_id()
    for name in RECORD_NAMES:
        data: dict = get_dns_record(zone_id, f"{name}.{ROOT_NAME}")
        cur_ip = data['content']
        if pub_ip != cur_ip:
            log.info(f"IP for Record {name} with ID: {data['id']} not synced. Record IP is {cur_ip} but should be "
                     f"{pub_ip} Attempting to change ...")
            change_dns_record(zone_id, data['id'], pub_ip)


if __name__ == '__main__':
    main()
