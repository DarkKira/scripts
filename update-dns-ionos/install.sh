#!/bin/bash
# Purpose: Install update-dns-records-ionos script
# Author: Lukas J. Schmidt
# -------------------------------------------------------

USER=updatedns
INSTALL_PATH=/opt/update-dns-ionos
LOG_PATH=/var/log/update-dns-ionos

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit 1
fi

apt update
apt install -y python3 python3-pip curl

pip install python-dotenv

useradd $USER

mkdir -p $INSTALL_PATH
mkdir -p $LOG_PATH

cd $INSTALL_PATH
curl https://gitlab.com/DarkKira/scripts/-/raw/main/update-dns-ionos/update-dns-ionos.py -o update-dns-ionos.py
curl https://gitlab.com/DarkKira/scripts/-/raw/main/update-dns-ionos/.env-empty -o .env-empty

chown $USER:$USER $INSTALL_PATH -R
chown $USER:$USER $LOG_PATH -R

cat <<EOF > /etc/systemd/system/update-dns-ionos.service
[Unit]
Description=Updates the dns records every 5 minutes

[Service]
Type=oneshot
User=$USER
Group=$USER
ExecStart=python3 $INSTALL_PATH/update-dns-ionos.py

EOF

cat <<EOF > /etc/systemd/system/update-dns-ionos.timer
[Unit]
Description=Run update-dns-ionos.service every 5 minutes

[Timer]
OnCalendar=*-*-* *:00,05,10,15,20,25,30,35,40,45,50,55:00
Persistent=true

[Install]
WantedBy=timers.target

EOF

systemctl enable update-dns-ionos.timer
systemctl enable update-dns-ionos.service
systemctl daemon-reload

exit 0
