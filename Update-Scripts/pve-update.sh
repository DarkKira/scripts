#!/bin/bash
# Purpose: Update pve
# Author: Lukas J. Schmidt
# -------------------------------------------------------

# Update package list
echo "*** [Updating $(hostname) @ $(date)] ***"
echo "Updating package list"
apt -y autoclean
apt update
if true
then
    echo "Purging old packages"
    apt autoremove --purge
fi

# If no packages are upgradable, then the message is "Listing... Done".
# Otherwise a package name is listed as upgradable.
needs_update=$(apt list --upgradable 2>/dev/null | grep -c -i -v 'Listing')

# Only update if packages are available
if [[ "$needs_update" -gt 0 ]]
then
    apt full-upgrade -y
    ret_val=$?
    if [[ "$ret_val" -eq 0 ]]
    then
        echo "Upgraded system"
    else
        echo "Failed to upgrade system, error $ret_val"
        exit "$ret_val"
    fi
else
    echo "No system updates"
fi

exit 0
