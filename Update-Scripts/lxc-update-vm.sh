#!/bin/bash
# Purpose: Update all lxc and vms
# Author: Lukas J. Schmidt
# -------------------------------------------------------

# Get the vm list
vms="$(lxc-ls --active)"

# Update each vm
update_vm(){
        local vm="$1"
        echo "*** [VM: $vm [$(hostname) @ $(date)] ] ***"
        /usr/bin/lxc-attach -n "$vm" apt -- update
        /usr/bin/lxc-attach -n "$vm" apt -- -y full-upgrade
        /usr/bin/lxc-attach -n "$vm" apt -- -y clean
        /usr/bin/lxc-attach -n "$vm" apt -- -y autoclean
        echo "-----------------------------------------------------------------"
}

# Do it
for v in $vms
do
   update_vm "$v"
done
