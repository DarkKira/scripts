#!/bin/bash
# Purpose: Update system
# -------------------------------------------------------

# Update package list
echo "*** [Updating $(hostname) @ $(date)] ***"
echo "Updating package list"
sudo apt -y autoclean
sudo apt update
if true
then
    echo "Purging old packages"
    sudo apt autoremove --purge
fi

# If no packages are upgradable, then the message is "Listing... Done".
# Otherwise a package name is listed as upgradable.
needs_update=$(sudo apt list --upgradable 2>/dev/null | grep -c -i -v 'Listing')

# Only update if packages are available
if [[ "$needs_update" -gt 0 ]]
then
    sudo apt full-upgrade -y
    ret_val=$?
    if [[ "$ret_val" -eq 0 ]]
    then
        echo "Upgraded system"
    else
        echo "Failed to upgrade system, error $ret_val"
        exit "$ret_val"
    fi
else
    echo "No system updates"
fi

# If brew is installed uncomment following line
# brew update && brew upgrade

# If brew is installed and zsh is beeing used uncomment following line
# brew update && brew upgrade && exec zsh

exit 0
